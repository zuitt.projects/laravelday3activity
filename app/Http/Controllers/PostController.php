<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Post;


class PostController extends Controller
{
  public function create()
  {
    return view('posts.create');
  }

  //let's create post controler to store the data to the database
  public function store(Request $request)
  {
      //if there is an authenticated user
    if(Auth::user()){
        //create a new Post objet from the Post model
        $post = new Post;

        //define the properties of the $post object using received from
        $post->title = $request->input('title');
        $post->content = $request->input('content');
        //get the id of the authenticated user and set it as the value of the user_id column
        $post->user_id = (Auth::user()->id);
        //save the post object to the database
        $post->save();

        return redirect('/posts');
    }else{
        return redirect('/login');
    }
  }

  //let's create method for index post from routes
public function index()
    {
        //get all posts from the database
        $posts = Post::all();
        return view('posts.index')->with('posts', $posts);
    }


}

//means need natin gumawa ng posts folder then create na file
