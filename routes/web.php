<?php

use Illuminate\Support\Facades\Route;
//dito lalagay
use App\Http\Controllers\HomeController;
use App\Http\Controllers\PostController;
use App\Http\Controllers\FeaturedPostController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


//route to return a view where the user can create a post
Route::get('/posts/create', [PostController::class, 'create']);

//route for a route wherein formm data can be sent via POST method

Route::post('/posts', [PostController::class, 'store']);


//route that will return a view containing all posts
Route::get('/posts', [PostController::class, 'index']);


Auth::routes();

Route::get('/home', [HomeController::class, 'index']);

//Featured Controller
Route::get('/', [FeaturedPostController::class, 'index']);

